// import jsonp from 'smol-jsonp'
import Vue from 'vue'
import VueJsonp from 'vue-jsonp'
global.VueJsonp=VueJsonp
Vue.use(VueJsonp)
// var jsonp2 = require('smol-jsonp')
// console.log(jsonp,jsonp==jsonp2)
import low from 'lowdb'
import LocalStorage from 'lowdb/adapters/LocalStorage'

const adapter = new LocalStorage('db')
const db = low(adapter)
global.db = db;
db.defaults({
    people: []
  })
  .write()
// db.defaults({ posts: [] })
//   .write()

// // Data is automatically saved to localStorage
// db.get('posts')
//   .push({ title: 'lowdb' })
//   .write()
const dataModule = {
  get(
    entity,
    filter
  ) {
    console.log('data.get', entity, filter)
    let results = db.get(entity)
    if (results) {
      console.log('data.get return', db.get(entity).value().filter(e => e != null))
      return db.get(entity).value().filter(e => e != null);
    }
    return []
  },
  async getFromFile(
    entity,
    filter
  ) {
    console.log('data.getFromFile', entity, filter)
    let result = await Vue.jsonp('data/mydata.js', {
      callbackQuery: 'cb',
      callbackName: 'callback'
    });

    // .then(result => {
    //   // Success.
      console.log('results from file', result)
    // }).catch(err => {
    //   // Failed.
    // })
    // if (results) {
    //   console.log('data.get return', db.get(entity).value().filter(e => e != null))
    //   return db.get(entity).value().filter(e => e != null);
    // }
  },
  // async getFromFile(
  //   entity,
  //   filter
  // ) {
  //   console.log('data.getFromFile', entity, filter)
  //   global.callback=results=>{
  //     console.log('results from file', results)
  //   }
  //   let results = await jsonp('data/mydata.js');

  //   console.log('results from file', results)
  //   // if (results) {
  //   //   console.log('data.get return', db.get(entity).value().filter(e => e != null))
  //   //   return db.get(entity).value().filter(e => e != null);
  //   // }
  // },
  add(
    entity,
    data
  ) {
    console.log('data.add', entity, data)
    if (data) {
      let _db = db.get(entity)
        .push(data).write();
    }
    console.log('data.add _db:', db.get(entity).value())
  },
  remove(entity, filter) {
    console.log('remove', entity, filter)
    if (filter == 'all') {
      db.set(entity, []).write();
    }
  }
}
global.data = dataModule;
export default dataModule;
